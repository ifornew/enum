<?php
namespace Ifornew\Enum;

/**
 * The interface implemented by C++ style enumeration instances.
 *
 * @api
 */
interface EnumerationInterface extends ValueMultitonInterface
{
}
